create table if not exists environments (
    id integer primary key autoincrement,
    name varchar(20),
    port int(5),
    color varchar(5),
    enabled enum(0, 1)
);

create table if not exists applications (
    id integer primary key autoincrement,
    name varchar(50)
);

create table if not exists versions (
    id integer primary key autoincrement,
    app_id integer not null,
    name varchar
);

create table if not exists env_app (
    id integer primary key autoincrement,
    env_id integer not null,
    app_id integer not null,
    version_id varchar(30),
    hash varchar(50)
);

create table if not exists hosts (
    id integer primary key autoincrement,
    ip varchar(20)
);

create table if not exists env_host (
    id integer primary key autoincrement,
    env_id integer not null,
    host_id integer not null
);
