insert or ignore into environments (id, name, port, color, enabled) values
    (1, 'dev', 8000, 'blue', 1),
    (2, 'dev', 8001, 'green', 0),
    (3, 'stage', 8002, 'blue', 1),
    (4, 'stage', 8003, 'green', 0),
    (5, 'new_ui', 8004, 'blue', 1),
    (6, 'new_ui', 8005, 'green', 0),
    (7, 'prod', 8006, 'blue', 1),
    (8, 'prod', 8007, 'green', 0);

insert or ignore into hosts (id, ip) values
    (1, 'node1'),
    (2, 'node2');

insert or ignore into applications (id, name) values
    (1, 'app'),
    (2, 'portal');

insert or ignore into versions (id, app_id, name) values
    (1, 1, 'v1.0.0'),
    (2, 2, 'v1.0.0');

insert or ignore into env_app (id, env_id, app_id, version_id) values
    (1, 1, 1, 1),
    (2, 1, 2, 2),
    (3, 2, 1, 1),
    (4, 2, 2, 2),
    (5, 3, 1, 1),
    (6, 3, 2, 2),
    (7, 4, 1, 1),
    (8, 4, 2, 2),
    (9, 5, 1, 1),
    (10, 5, 2, 2),
    (11, 6, 1, 1),
    (12, 6, 2, 2),
    (13, 7, 1, 1),
    (14, 7, 2, 2),
    (15, 8, 1, 1),
    (16, 8, 2, 2);

insert or ignore into env_host (id, env_id, host_id) values
    (1, 1, 1),
    (2, 2, 1),
    (3, 3, 1),
    (4, 4, 1),
    (5, 5, 1),
    (6, 6, 1),
    (7, 7, 2),
    (8, 8, 2);
