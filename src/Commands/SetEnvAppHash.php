<?php

namespace App\Commands;


use App\Entities\Application;
use App\Entities\EnvAppState;
use App\Entities\Environment;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetEnvAppHash extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('set-env-app-hash')
            ->setDescription('Set application hash in specific environment')
            ->addArgument('environment', InputArgument::REQUIRED)
            ->addArgument('color', InputArgument::REQUIRED)
            ->addArgument('application', InputArgument::REQUIRED)
            ->addArgument('hash', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $environmentArg = $input->getArgument('environment');
        $colorArg = $input->getArgument('color');
        $applicationArg = $input->getArgument('application');
        $hashArg = $input->getArgument('hash');

        $environmentRepository = $this->entityManager->getRepository(Environment::class);
        $applicationRepository = $this->entityManager->getRepository(Application::class);
        $stateRepository = $this->entityManager->getRepository(EnvAppState::class);

        $environment = $environmentRepository->findOneBy([
            'color' => $colorArg,
            'name' => $environmentArg
        ]);

        if (!$environment) {
            $this->envNotFound($output, $environmentArg);
            exit(1);
        }

        $application = $applicationRepository->findOneBy([ 'name' => $applicationArg]);

        if (!$application) {
            $this->appNotFound($output, $applicationArg);
            exit(1);
        }

        /**
         * @var $state EnvAppState
         */
        $state = $stateRepository->findOneBy([
            'environment' => $environment,
            'application' => $application
        ]);

        if (!$state) {
            $state = new EnvAppState();
            $state->setEnvironment($environment);
            $state->setApplication($application);
            $state->setVersion(null);
            $state->setHash($hashArg);

            $this->entityManager->persist($state);
        } else {
            $state->setVersion(null);
            $state->setHash($hashArg);
        }

        $this->entityManager->flush();

        $output->writeln("Hash of application '$applicationArg' in environment '$environmentArg' ($colorArg) " .
            "has been changed to '$hashArg'");


    }
}