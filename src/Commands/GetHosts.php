<?php

namespace App\Commands;


use App\Entities\Host;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetHosts extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('get-hosts')
            ->setDescription('Get all hosts');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $hostRepository = $this->entityManager->getRepository(Host::class);

        $hosts = $hostRepository->findAll();

        if (count($hosts) === 0) {
            $output->writeln('There are no hosts');
            exit(1);
        }

        $this->echoTitle($output, 'Hosts');

        foreach ($hosts as $host) {

            /**
             * @var $host Host
             */
            $output->writeln(' - ' . $host->getIp());
        }
    }
}