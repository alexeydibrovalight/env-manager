<?php

namespace App\Commands;

use App\Entities\Application;
use App\Entities\Version;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckVersionExists extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('check-app-version')
            ->setDescription('Checks if an application\'s version exists')
            ->addArgument('application')
            ->addArgument('version');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $applicationArg = $input->getArgument('application');
        $versionArg = $input->getArgument('version');

        $applicationRepository = $this->entityManager->getRepository(Application::class);

        /**
         * @var $application Application
         */
        $application = $applicationRepository->findOneBy(['name' => $applicationArg]);

        if (!$application) {
            $this->appNotFound($output, $applicationArg);
            exit(1);
        }

        $versions = $application->getVersions();

        $matchedVersions = $versions->filter(function(Version $version) use ($versionArg) {
            return $version->getName() === $versionArg;
        });

        if ($matchedVersions->count() === 0) {
            $output->writeln("Version '$versionArg' is not found for '$applicationArg' application");
            exit(1);
        }

        $output->writeln("Version '$versionArg' exists");
    }
}