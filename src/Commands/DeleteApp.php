<?php

namespace App\Commands;


use App\Entities\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteApp extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('delete-app')
            ->setDescription('Delete application')
            ->addArgument('application');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $applicationArg = $input->getArgument('application');

        $applicationRepository = $this->entityManager->getRepository(Application::class);

        /**
         * @var $application Application
         */
        $application = $applicationRepository->findOneBy([
            'name' => $applicationArg
        ]);

        if (!$application) {
            $this->appNotFound($output, $applicationArg);
            exit(1);
        }

        $state = $application->getState();

        if ($state) {
            $output->writeln("Application '$applicationArg' is in use");
            exit(1);
        }

        $this->entityManager->remove($application);
        $this->entityManager->flush();

        $output->writeln("Application '$applicationArg' has been successfully removed");
    }
}