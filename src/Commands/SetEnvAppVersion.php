<?php

namespace App\Commands;


use App\Entities\Application;
use App\Entities\EnvAppState;
use App\Entities\Environment;
use App\Entities\Version;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetEnvAppVersion extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('set-env-app-version')
            ->setDescription('Change an application version for an environment')
            ->addArgument('environment', InputArgument::REQUIRED)
            ->addArgument('color', InputArgument::REQUIRED)
            ->addArgument('application', InputArgument::REQUIRED)
            ->addArgument('version', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $environmentArg = $input->getArgument('environment');
        $colorArg = $input->getArgument('color');
        $applicationArg = $input->getArgument('application');
        $versionArg = $input->getArgument('version');

        $environmentRepository = $this->entityManager->getRepository(Environment::class);
        $applicationRepository = $this->entityManager->getRepository(Application::class);
        $versionRepository = $this->entityManager->getRepository(Version::class);
        $stateRepository = $this->entityManager->getRepository(EnvAppState::class);


        $environment = $environmentRepository->findOneBy([
            'color' => $colorArg,
            'name' => $environmentArg
        ]);

        if (!$environment) {
            $this->envNotFound($output, $environmentArg);
            exit(1);
        }

        $application = $applicationRepository->findOneBy([ 'name' => $applicationArg]);

        if (!$application) {
            $this->appNotFound($output, $applicationArg);
            exit(1);
        }

        $version = $versionRepository->findOneBy([
            'application' => $application,
            'name' => $versionArg
        ]);

        if (!$version) {
            $this->verNotFound($output, $versionArg);
            exit(1);
        }

        /**
         * @var $state EnvAppState
         */
        $state = $stateRepository->findOneBy([
            'environment' => $environment,
            'application' => $application
        ]);

        if (!$state) {
            $state = new EnvAppState();
            $state->setEnvironment($environment);
            $state->setApplication($application);
            $state->setVersion($version);
            $state->setHash('');

            $this->entityManager->persist($state);
        } else {
            $state->setVersion($version);
            $state->setHash('');
        }

        $this->entityManager->flush();

        $output->writeln("Version of application '$applicationArg' in environment '$environmentArg' ($colorArg) " .
        "has been changed to '$versionArg'");

    }
}