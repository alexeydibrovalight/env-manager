<?php

namespace App\Commands;


use App\Entities\Host;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddHost extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('add-host')
            ->setDescription('Add host')
            ->addArgument('ip');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ipArg = $input->getArgument('ip');

        $hostRepository = $this->entityManager->getRepository(Host::class);

        $isExists = $hostRepository->findOneBy([
            'ip' => $ipArg
        ]);

        if ($isExists) {
            $output->writeln("Host '$ipArg' is already exist");
            exit(1);
        }

        $host = new Host();
        $host->setIp($ipArg);

        $this->entityManager->persist($host);
        $this->entityManager->flush();

        $output->writeln("Host '$ipArg' has been successfully added");
    }
}