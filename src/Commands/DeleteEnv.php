<?php

namespace App\Commands;

use App\Entities\Environment;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteEnv extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('delete-env')
            ->setDescription('Delete environment')
            ->addArgument('environment');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $environmentArg = $input->getArgument('environment');

        $environmentRepository = $this->entityManager->getRepository(Environment::class);

        $environments = $environmentRepository->findBy([
            'name' => $environmentArg
        ]);

        if (!$environments) {
            $this->envNotFound($output, $environmentArg);
            exit(1);
        }

        foreach ($environments as $environment) {
            $this->entityManager->remove($environment);
        }

        $this->entityManager->flush();

        $output->writeln("Environment '$environmentArg' has been successfully removed");
    }

}