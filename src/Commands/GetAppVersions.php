<?php

namespace App\Commands;

use App\Entities\Application;
use App\Entities\Version;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetAppVersions extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('get-app-versions')
            ->setDescription('Shows all application\'s versions')
            ->addArgument('application', InputArgument::REQUIRED, 'Application name');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $applicationArg = $input->getArgument('application');

        $repository = $this->entityManager->getRepository(Application::class);

        /**
         * @var $application Application
         */
        $application = $repository->findOneBy(['name' => $applicationArg]);

        if (!$application) {
            $this->appNotFound($output, $applicationArg);
            exit(1);
        }

        $versions = $application->getVersions();

        if ($versions->isEmpty()) {
            $output->writeln("Application '$applicationArg' hasn't any version");
            exit(1);
        }

        $this->echoTitle($output, 'Versions');

        foreach ($versions as $version) {
            /**
             * @var $version Version
             */
            $output->writeln($version->getName());
        }
    }
}