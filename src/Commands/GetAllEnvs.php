<?php

namespace App\Commands;

use App\Entities\EnvAppState;
use App\Entities\Environment;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GetAllEnvs extends EmCommand {

    protected function configure()
    {
        $this
            ->setName('get-envs')
            ->setDescription('Show all environments')
            ->addOption('enabled', null, InputOption::VALUE_NONE, 'Shows only enabled environments');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->echoTitle($output, 'Environments');

        $enabled = $input->getOption('enabled');

        $repository = $this->entityManager
            ->getRepository(Environment::class);

        /**
         * @var $environments Environment[]
         */
        $environments = $enabled
            ? $repository->findBy(['enabled' => 1])
            : $repository->findAll();

        foreach ($environments as $environment) {

            $output->writeln([
                'Name - ' . $environment->getName(),
                'Host - ' . $environment->getHostState()->getHost()->getIp(),
                'Port - ' . $environment->getPort(),
                'Color - ' . $environment->getColor()
            ]);

            $states = $environment->getStates();

            $output->writeln('Applications:');

            foreach ($states as $state) {
                /**
                 * @var $state EnvAppState
                 */
                $application = $state->getApplication();
                $version = $state->getVersion();

                $versionStr = $version ? $version->getName() : $state->getHash() . ' (hash)';

                $output->writeln(' - ' . $application->getName() . ': ' . $versionStr);
            }


            if (!$enabled) {
                $output->writeln('Enabled - ' . ($environment->getEnabled() ? 'true' : 'false'));
            }

            $output->writeln('------------------------------');
        }
    }
    
}
