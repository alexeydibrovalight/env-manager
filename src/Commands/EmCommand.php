<?php

namespace App\Commands;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;

abstract class EmCommand extends Command
{
    /**
     * @var $entityManager EntityManager
     */
    protected $entityManager;

    public function __construct($name = null, EntityManager $entityManager = null)
    {
        parent::__construct($name);

        $this->entityManager = $entityManager;
    }

    protected function echoTitle(OutputInterface $output, string $title)
    {
        $output->writeln([
            '==============================',
            '  ' . $title,
            '=============================='
        ]);
    }

    protected function envNotFound(OutputInterface $output, $name)
    {
        $output->writeln($this->getNotFoundMessage('Environment', $name));
    }

    protected function appNotFound(OutputInterface $output, $name)
    {
        $output->writeln($this->getNotFoundMessage('Application', $name));
    }

    protected function verNotFound(OutputInterface $output, $name)
    {
        $output->writeln($this->getNotFoundMessage('Version', $name));
    }

    protected function hostNotFound(OutputInterface $output, $name)
    {
        $output->writeln($this->getNotFoundMessage('Host', $name));
    }

    protected function getNotFoundMessage($what, $name)
    {
        return "$what '$name' is not found";
    }
}