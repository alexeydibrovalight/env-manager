<?php

namespace App\Commands;


use App\Entities\Application;
use App\Entities\EnvAppState;
use App\Entities\Environment;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetEnvAppHash extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('get-env-app-hash')
            ->setDescription('Get application hash')
            ->addArgument('environment', InputArgument::REQUIRED)
            ->addArgument('color', InputArgument::REQUIRED)
            ->addArgument('application', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $environmentArg = $input->getArgument('environment');
        $colorArg = $input->getArgument('color');
        $applicationArg = $input->getArgument('application');

        $environmentRepository = $this->entityManager->getRepository(Environment::class);
        $applicationRepository = $this->entityManager->getRepository(Application::class);
        $stateRepository = $this->entityManager->getRepository(EnvAppState::class);

        $environment = $environmentRepository->findOneBy([
            'color' => $colorArg,
            'name' => $environmentArg
        ]);

        if (!$environment) {
            $this->envNotFound($output, $environmentArg);
            exit(1);
        }

        $application = $applicationRepository->findOneBy([ 'name' => $applicationArg]);

        if (!$application) {
            $this->appNotFound($output, $applicationArg);
            exit(1);
        }

        /**
         * @var $state EnvAppState
         */
        $state = $stateRepository->findOneBy([
            'environment' => $environment,
            'application' => $application
        ]);

        $hash = $state->getHash();

        $output->write($hash);
    }
}