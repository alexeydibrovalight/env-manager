<?php

namespace App\Commands;


use App\Entities\EnvAppState;
use App\Entities\Environment;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddEnv extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('add-env')
            ->setDescription('Add new environment')
            ->addArgument('environment');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $environmentArg = $input->getArgument('environment');

        $environmentRepository = $this->entityManager->getRepository(Environment::class);

        $isExists = $environmentRepository->findOneBy([
            'name' => $environmentArg
        ]);

        if ($isExists) {
            $output->writeln("Environment '$environmentArg' is already exist.");
            exit(1);
        }

        $maxPort = $environmentRepository->createQueryBuilder('e')
            ->select('max(e.port)')
            ->getQuery()
            ->getSingleScalarResult();

        $environmentBlue = new Environment();
        $environmentBlue->setName($environmentArg);
        $environmentBlue->setColor('blue');
        $environmentBlue->setEnabled(1);
        $environmentBlue->setPort($maxPort + 1);

        $environmentGreen = new Environment();
        $environmentGreen->setName($environmentArg);
        $environmentGreen->setColor('green');
        $environmentGreen->setEnabled(0);
        $environmentGreen->setPort($maxPort + 2);

        $this->entityManager->persist($environmentBlue);
        $this->entityManager->persist($environmentGreen);

        $this->entityManager->flush();

        $output->writeln("Environment '$environmentArg' has been successfully added");
    }

}