<?php

namespace App\Commands;

use App\Entities\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddApp extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('add-app')
            ->setDescription('Add application')
            ->addArgument('application');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $applicationArg = $input->getArgument('application');

        $applicationRepository = $this->entityManager->getRepository(Application::class);

        $isExists = $applicationRepository->findOneBy([
            'name' => $applicationArg
        ]);

        if ($isExists) {
            $output->writeln("Application '$applicationArg' is already exist");
            exit(1);
        }

        $application = new Application();
        $application->setName($applicationArg);

        $this->entityManager->persist($application);
        $this->entityManager->flush();

        $output->writeln("Application '$applicationArg' has been successfully created");
    }
}