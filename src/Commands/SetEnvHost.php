<?php

namespace App\Commands;

use App\Entities\EnvHost;
use App\Entities\Environment;
use App\Entities\Host;
use Doctrine\Common\Util\Debug;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetEnvHost extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('set-env-host')
            ->setDescription('Change environment\'s host')
            ->addArgument('environment')
            ->addArgument('ip');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $environmentArg = $input->getArgument('environment');
        $ipArg = $input->getArgument('ip');

        $environmentRepository = $this->entityManager->getRepository(Environment::class);
        $hostRepository = $this->entityManager->getRepository(Host::class);

        $environments = $environmentRepository->findBy([
            'name' => $environmentArg
        ]);

        /**
         * @var $host Host
         */
        $host = $hostRepository->findOneBy([
            'ip' => $ipArg
        ]);

        if (!$host) {
            $this->hostNotFound($output, $ipArg);
            exit(1);
        }

        foreach ($environments as $environment) {
            /**
             * @var $environment Environment
             */
            $hostState = $environment->getHostState();

            if (!$hostState) {
                $hostState = new EnvHost();
                $hostState->setEnvironment($environment);
                $hostState->setHost($host);
                $this->entityManager->persist($hostState);
            } else {
                $hostState->setHost($host);
            }

            $this->entityManager->flush();
        }

        $output->writeln("Host '$ipArg' has been successfully established for '$environmentArg' environment");
    }
}