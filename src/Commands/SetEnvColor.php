<?php

namespace App\Commands;


use App\Entities\Environment;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetEnvColor extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('set-env-color')
            ->setDescription('Change current environment\'s color')
            ->addArgument('environment', InputArgument::REQUIRED)
            ->addArgument('color', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $environmentArg = $input->getArgument('environment');
        $colorArg = $input->getArgument('color');

        if (!in_array($colorArg, ['blue', 'green'])) {
            $output->writeln('Valid colors are \'blue\' and \'green\' only');
            exit(1);
        }

        $environmentRepository = $this->entityManager->getRepository(Environment::class);

        $newColor = $colorArg;

        $oldColor = $newColor === 'blue'? 'green': 'blue';

        /**
         * @var $oldEnvironment Environment
         */
        $oldEnvironment = $environmentRepository->findOneBy([
            'name' => $environmentArg,
            'color' => $oldColor
        ]);

        /**
         * @var $newEnvironment Environment
         */
        $newEnvironment = $environmentRepository->findOneBy([
            'name' => $environmentArg,
            'color' => $newColor
        ]);

        if (!$oldEnvironment || !$newEnvironment) {
            $this->envNotFound($output, $environmentArg);
            exit(1);
        }

        $oldEnvironment->setEnabled(false);
        $newEnvironment->setEnabled(true);

        $this->entityManager->flush();

        $output->writeln("Color for the environment '$environmentArg' has been changed to '$colorArg'");
    }
}