<?php

namespace App\Commands;


use App\Entities\Application;
use App\Entities\EnvAppState;
use App\Entities\Version;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Util\Debug;

class DeleteAppVersion extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('delete-app-version')
            ->setDescription('Delete application version')
            ->addArgument('application')
            ->addArgument('version');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $applicationArg = $input->getArgument('application');
        $versionArg = $input->getArgument('version');

        $applicationRepository = $this->entityManager->getRepository(Application::class);
        $stateRepository = $this->entityManager->getRepository(EnvAppState::class);

        /**
         * @var $application Application
         */
        $application = $applicationRepository->findOneBy([
            'name' => $applicationArg
        ]);

        if (!$application) {
            $this->appNotFound($output, $applicationArg);
            exit(1);
        }


        $applicationVersions = $application->getVersions();

        $versionMatches = $applicationVersions->filter(
            function (Version $appVersion) use ($versionArg) {
                return $appVersion->getName() === $versionArg;
            }
        );

        if ($versionMatches->count() === 0) {
            $this->verNotFound($output, $versionArg);
            exit(1);
        }

        $version = $versionMatches->first();

        $isExistsStates = $stateRepository->findOneBy([
            'version' => $version
        ]);

        if ($isExistsStates) {
            $output->writeln("Version '$versionArg' is in use");
            exit(1);
        }

        $this->entityManager->remove($version);
        $this->entityManager->flush();

        $output->writeln("Version '$versionArg' of application '$applicationArg' has been successfully removed");
    }
}
