<?php

namespace App\Commands;


use App\Entities\Application;
use App\Entities\Version;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddAppVersion extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('add-app-version')
            ->setDescription('Add new version to application')
            ->addArgument('application', InputArgument::REQUIRED)
            ->addArgument('version', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $applicationArg = $input->getArgument('application');
        $versionArg = $input->getArgument('version');

        $applicationRepository = $this->entityManager->getRepository(Application::class);
        $versionRepository = $this->entityManager->getRepository(Version::class);

        /**
         * @var $application Application
         */
        $application = $applicationRepository->findOneBy(['name' => $applicationArg]);

        if (!$application) {
            $this->appNotFound($output, $applicationArg);
            exit(1);
        }

        $isVersionExists = $versionRepository->findOneBy([
            'application' => $application,
            'name' => $versionArg
        ]);

        if ($isVersionExists) {
            $output->writeln("Version '$versionArg' is already exist");
            exit(0);
        }

        $version = new Version();
        $version->setName($versionArg);
        $version->setApplication($application);

        $this->entityManager->persist($version);
        $this->entityManager->flush();

        $output->writeln("Version '$versionArg' was successfully added");
    }
}
