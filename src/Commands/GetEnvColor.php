<?php

namespace App\Commands;


use App\Entities\Environment;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetEnvColor extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('get-env-color')
            ->setDescription('Get current environment\'s color')
            ->addArgument('environment', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $environmentArg = $input->getArgument('environment');

        $environmentRepository = $this->entityManager->getRepository(Environment::class);

        /**
         * @var $environment Environment
         */
        $environment = $environmentRepository->findOneBy([
            'name' => $environmentArg,
            'enabled' => 1
        ]);

        if (!$environment) {
            $this->envNotFound($output, $environmentArg);
            exit(1);
        }

        $color = $environment->getColor();

        $output->write($color);
    }

}