<?php

namespace App\Commands;


use App\Entities\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetApps extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('get-apps')
            ->setDescription('List of all applications');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $applications = $this->entityManager->getRepository(Application::class)
            ->findAll();

        if (count($applications) === 0) {
            $output->writeln('There are no applications');
            exit(1);
        }

        $this->echoTitle($output, 'Applications');

        foreach ($applications as $application) {
            /**
             * @var $application Application
             */
            $output->writeln(' - ' . $application->getName());
        }
    }
}