<?php

namespace App\Commands;


use App\Entities\Host;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteHost extends EmCommand
{
    protected function configure()
    {
        $this
            ->setName('delete-host')
            ->setDescription('Delete host')
            ->addArgument('ip');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ipArg = $input->getArgument('ip');

        $hostRepository = $this->entityManager->getRepository(Host::class);

        /**
         * @var $host Host
         */
        $host = $hostRepository->findOneBy([
            'ip' => $ipArg
        ]);

        if (!$host) {
            $output->writeln("Host '$ipArg' is not found");
            exit(1);
        }

        $hostStates = $host->getHostStates();

        if ($hostStates->count() > 0) {
            $output->writeln("Host '$ipArg' is already in use");
            exit(1);
        }

        $this->entityManager->remove($host);
        $this->entityManager->flush();

        $output->writeln("Host '$ipArg' has been successfully removed");
    }
}