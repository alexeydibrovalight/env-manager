<?php

namespace App\Commands;

use App\Commands\EmCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entities\Environment;

class GetEnvHost extends EmCommand {

    protected function configure()
    {
        $this
            ->setName('get-env-host')
            ->setDescription('Get environment host')
            ->addArgument('environment')
            ->addArgument('color');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $environmentArg = $input->getArgument('environment');
        $colorArg = $input->getArgument('color');

        $environmentRepository = $this->entityManager->getRepository(Environment::class);

        $environment = $environmentRepository->findOneBy([
            'name' => $environmentArg,
            'color' => $colorArg
        ]);

        if (!$environment) {
            $this->envNotFound($output, $environmentArg);
            exit(1);
        }

        $output->write($environment->getHostState()->getHost()->getIp());
    }
    
}
