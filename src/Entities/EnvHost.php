<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class EnvHost
 * @package App\Entities
 *
 * @ORM\Entity
 * @ORM\Table(name="env_host")
 */
class EnvHost
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Environment
     *
     * @ORM\OneToOne(targetEntity="App\Entities\Environment", inversedBy="hostState")
     * @ORM\JoinColumn(name="env_id", referencedColumnName="id")
     */
    private $environment;

    /**
     * @var Host
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\Host", inversedBy="hostStates")
     * @ORM\JoinColumn(name="host_id", referencedColumnName="id")
     */
    private $host;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Environment
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * @param Environment $environment
     */
    public function setEnvironment($environment)
    {
        $this->environment = $environment;
    }

    /**
     * @return Host
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param Host $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }
}