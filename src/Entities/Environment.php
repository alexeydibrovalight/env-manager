<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Environment
 *
 * @ORM\Entity
 * @ORM\Table(name="environments")
 */
class Environment
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $port;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $color;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $enabled;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection|null
     *
     * @ORM\OneToMany(targetEntity="App\Entities\EnvAppState", mappedBy="environment")
     */
    private $states;

    /**
     * @var EnvHost
     *
     * @ORM\OneToOne(targetEntity="App\Entities\EnvHost", mappedBy="environment")
     */
    private $hostState;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->states = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Environment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set port.
     *
     * @param int $port
     *
     * @return Environment
     */
    public function setPort($port)
    {
        $this->port = $port;

        return $this;
    }

    /**
     * Get port.
     *
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Set color.
     *
     * @param string $color
     *
     * @return Environment
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color.
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set enabled.
     *
     * @param int $enabled
     *
     * @return Environment
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled.
     *
     * @return int
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Add state.
     *
     * @param \App\Entities\EnvAppState $state
     *
     * @return Environment
     */
    public function addState(\App\Entities\EnvAppState $state)
    {
        $this->states[] = $state;

        return $this;
    }

    /**
     * Remove state.
     *
     * @param \App\Entities\EnvAppState $state
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeState(\App\Entities\EnvAppState $state)
    {
        return $this->states->removeElement($state);
    }

    /**
     * Get states.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStates()
    {
        return $this->states;
    }

    /**
     * @return EnvHost
     */
    public function getHostState()
    {
        return $this->hostState;
    }

    /**
     * @param EnvHost $hostState
     */
    public function setHostState($hostState)
    {
        $this->hostState = $hostState;
    }
}
