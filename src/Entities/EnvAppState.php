<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * EnvAppState
 *
 * @ORM\Entity
 * @ORM\Table(name="env_app")
 */
class EnvAppState
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var \App\Entities\Environment
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\Environment", inversedBy="states")
     * @ORM\JoinColumn(name="env_id", referencedColumnName="id")
     */
    private $environment;

    /**
     * @var \App\Entities\Application
     *
     * @ORM\OneToOne(targetEntity="App\Entities\Application", inversedBy="state")
     * @ORM\JoinColumn(name="app_id", referencedColumnName="id")
     */
    private $application;

    /**
     * @var \App\Entities\Version
     *
     * @ORM\OneToOne(targetEntity="App\Entities\Version", inversedBy="state")
     * @ORM\JoinColumn(name="version_id", referencedColumnName="id")
     */
    private $version;

    /**
     * @var $hash string
     *
     * @ORM\Column(type="string")
     */
    private $hash;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set environment.
     *
     * @param \App\Entities\Environment|null $environment
     *
     * @return EnvAppState
     */
    public function setEnvironment(\App\Entities\Environment $environment = null)
    {
        $this->environment = $environment;

        return $this;
    }

    /**
     * Get environment.
     *
     * @return \App\Entities\Environment|null
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * Set application.
     *
     * @param \App\Entities\Application|null $application
     *
     * @return EnvAppState
     */
    public function setApplication(\App\Entities\Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application.
     *
     * @return \App\Entities\Application|null
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set version.
     *
     * @param \App\Entities\Version|null $version
     *
     * @return EnvAppState
     */
    public function setVersion(\App\Entities\Version $version = null)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version.
     *
     * @return \App\Entities\Version|null
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set hash
     *
     * @param $hash string
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }
}
