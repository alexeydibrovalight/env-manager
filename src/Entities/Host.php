<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Host
 * @package App\Entities
 *
 * @ORM\Entity
 * @ORM\Table(name="hosts")
 */
class Host
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $ip;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="App\Entities\EnvHost", mappedBy="host")
     */
    private $hostStates;

    public function __construct()
    {
        $this->hostStates = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return mixed
     */
    public function getHostStates()
    {
        return $this->hostStates;
    }

    /**
     * @param mixed $hostStates
     */
    public function setHostStates(\Doctrine\Common\Collections\ArrayCollection $hostStates)
    {
        $this->hostStates = $hostStates;
    }

}