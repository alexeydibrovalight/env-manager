<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Application
 *
 * @ORM\Entity
 * @ORM\Table(name="applications")
 */
class Application
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entities\Version", mappedBy="application", orphanRemoval=true)
     */
    private $versions;

    /**
     * @var \App\Entities\EnvAppState
     *
     * @ORM\OneToOne(targetEntity="App\Entities\EnvAppState", mappedBy="application")
     */
    private $state;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->versions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Application
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add version.
     *
     * @param \App\Entities\Version $version
     *
     * @return Application
     */
    public function addVersion(\App\Entities\Version $version)
    {
        $this->versions[] = $version;

        return $this;
    }

    /**
     * Remove version.
     *
     * @param \App\Entities\Version $version
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVersion(\App\Entities\Version $version)
    {
        return $this->versions->removeElement($version);
    }

    /**
     * Get versions.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVersions()
    {
        return $this->versions;
    }

    /**
     * Set state.
     *
     * @param \App\Entities\EnvAppState|null $state
     *
     * @return Application
     */
    public function setState(\App\Entities\EnvAppState $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state.
     *
     * @return \App\Entities\EnvAppState|null
     */
    public function getState()
    {
        return $this->state;
    }
}
