<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Version
 *
 * @ORM\Entity
 * @ORM\Table(name="versions")
 */
class Version
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var \App\Entities\Application
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\Application", inversedBy="versions")
     * @ORM\JoinColumn(name="app_id", referencedColumnName="id")
     */
    private $application;

    /**
     * @var \App\Entities\EnvAppState
     *
     * @ORM\OneToOne(targetEntity="App\Entities\EnvAppState", mappedBy="version")
     */
    private $state;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Version
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set application.
     *
     * @param \App\Entities\Application|null $application
     *
     * @return Version
     */
    public function setApplication(\App\Entities\Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application.
     *
     * @return \App\Entities\Application|null
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set state.
     *
     * @param \App\Entities\EnvAppState|null $state
     *
     * @return Version
     */
    public function setState(\App\Entities\EnvAppState $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state.
     *
     * @return \App\Entities\EnvAppState|null
     */
    public function getState()
    {
        return $this->state;
    }
}
